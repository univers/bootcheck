#!/bin/bash

VERSION='0.21.0'

################################################################################
#                                                                              #
# Copyright 2018, Adaptation  d'une idée  d'un developpeur.                    #
#                                                                              #
# Ce script est prévu pour être utilisé avec ubuntu                            #
#                                                                              #
# Ses sorties sont adaptées au forum français de ubuntu                        #
#                                                                              #
# Aucune traduction dans une autre langue n'est prévue.                        #
#                                                                              #                                                                            
################################################################################

## Check bash interpreter
if [ -z "$BASH_VERSION" ] ; then
   echo 'bootcheck needs to be run with bash as shell interpreter.' >&2;
   exit 1;
fi
Help () {
  cat <<- HELP
  Usage :
  bootcheck --options  COMMANDES
  Command options:
    -h, -help, --help
          Print this page.
    --ano
          inclure les anomalies rencontrées
    --luks
          Unlock LUKS devices before proceeding (live session only).
    --usb
          Print usb section.      
    xxxxx [ xxxx  ......]
          Print section XXXXX   where XXXXX= BOOT or DISK or EFI or MATOS or PACK or RESUME
    -v, -V, --version
          Print version information.
HELP
  exit 0;
}
## Print bootcheck version
Version () {
  printf '\nbootcheck version: %s\n\n' "${VERSION}";
  exit 0;
}
rootdevices=""
devices=""
SummaryId=""
SummaryFile=""

# Traitement d'édition des anomalies
ano=0

# GPU logs
gpu=0

# Edition des anomalies
ano=0

# Live session
livesession=0

# déjà un titre d'écrit
Titre=0

# Locales
locales=0

# Internet connection available
online=0

# usb devices
usb=0

# Working directory
WorkingDirectory=$(pwd)
# Temp directory
rm -R /tmp/bootcheck-* 2>> /dev/null
Tempdir=$(mktemp -t -d bootcheck-XXXX);
# Files for processing
cd "${Tempdir}"
Summary=${Tempdir}/Summary.txt
Errors=${Tempdir}/Errors
Trash=${Tempdir}/Trash
# Redirect errors
exec 2>> "${Errors}";

# Disks
NvmeDisks=$(ls -l /dev/disk/by-id/ | awk '{ print $9, $11 }' | sed '/^nvme/!d; /part/d; /\/nvme/!d; s/^.*\(nvme...*\)$/\1/' | sort -u | sed 's:^:/dev/:' | sed ':loop;N;$!bloop;s/\n/ /g')
MmcDisks=$(ls -l /dev/disk/by-id/ | awk '{ print $9, $11 }' | sed '/^mmc/!d; /part/d; /\/mmcblk/!d; s/^.*\(mmcblk..*\)$/\1/' | sort -u | sed 's:^:/dev/:' | sed ':loop;N;$!bloop;s/\n/ /g')
AtaDisks=$(ls -l /dev/disk/by-id/ | awk '{ print $9, $11 }' | sed '/^ata/!d; /part/d; /\/sd/!d; s/^.*\(sd..*\)$/\1/' | sort -u | sed 's:^:/dev/:' | sed ':loop;N;$!bloop;s/\n/ /g')
UsbDisks=$(ls -l /dev/disk/by-id/ | awk '{ print $9, $11 }' | sed '/^usb/!d; /part/d; /\/sd/!d; s/^.*\(sd..*\)$/\1/' | sort -u | sed 's:^:/dev/:' | sed ':loop;N;$!bloop;s/\n/ /g')

# SUDO
if [[ "${EUID}" -ne 0 ]] && hash sudo; then
  SUDO="sudo"
  ${SUDO} tail /dev/null
elif hash sudo; then
  #do nothing
  :    
else
  printf "sudo program not available. You may run bootcheck  as user root instead.\n"
  exit 0
fi

if ping -W3 -c1 github.com &> /dev/null; then
  online=1
fi

if (mount | grep -E 'aufs|overlay' | grep -q 'on / type'); then
  livesession=1
fi

# Unlock LUKS partitions
[[ "${luks}" -eq 1 ]] && [[ "${livesession}" -eq 0 ]] && \
printf -- "--luks command option only applies in a live session.\n" && exit 1

if [[ "${livesession}" -eq 1 ]] && [[ "${luks}" -eq 1 ]]; then
  if hash cryptsetup; then
    luksblk=$(${SUDO} blkid | grep LUKS | gawk '{print $1}' | sed 's/:$//')
    for lukspart in ${luksblk}; do
      if ! ls /dev/mapper | grep -q $(${SUDO} udisksctl info --block-device "${lukspart}" | grep "IdUUID" | gawk '{print $2}'); then
        printf "Enter passphrase to unlock LUKS device %s\n" "${lukspart}"
        LC_ALL=C udisksctl unlock --block-device "${lukspart}" || exit 1
      fi
    done
  else
    printf "cryptsetup program not available, can't unlock LUKS devices."
    exit 1
  fi
  unset luksblk
fi

CreateTitle () {
  local Title Line 
if  [[ "${Titre}" -eq 1 ]]; then
    if   test  "$2" != "_" ; then         
                printf "[/code] [code]"        
    fi
else   
    printf " [code]" ; Titre=1
fi
  Title=${1}
  Line=${2}
  printf "\n%$(( (80-${#Title}) / 2 - 4 ))s" | sed "s/ /${Line:-=}/g"
  printf "   ${Title}   "
# printf "%$(( (80-${#Title}) / 2 - 3 ))s\n\n" | sed "s/ /${Line:-=}/g"
  printf "%$(( (80-${#Title}) / 2 - 3 ))s\n"   | sed "s/ /${Line:-=}/g"
}

InxiSummary () {
  local inxisummary inxioptions
  ### inxi summary
  CreateTitle "Etat du matériel demandé"
  if which inxi &> /dev/null; then
     ### Mise en forme en séparant les réponses et en en selectonnant que certaines
     #   inxi -c0 -! 31 -ACGIMRSxxzdlnsutc5  | sed -r 's/^Machine|^Battery|^CPU|^Memory|^Graphics|^Audio|^Network|^Drives|^Partition|^RAID|^Sensors|^Repos|^[[:blank:]]+Active|^Processes|^Info/\n&/' 
    inxi -c0 -! 31 -ACGIMRSxxzdlsutc5  | sed -r 's/^Machine|^Battery|^CPU|^Memory|^Graphics|^Audio|^Drives|^Partition|^RAID|^Sensors|^Repos|^[[:blank:]]+Active|^Processes|^Info/\n&/'
     CreateTitle "inxi"    "_"; echo "$inxisummary "   
  else
      # inxisummary="inxi program not available on this system. Please install inxi."
      ixisummary=" "     
      ## CreateTitle "lshw -sanitize"    "_"; $SUDO lshw -sanitize   >> "${Summary}"   Cette commande semble lister trop de choses
      CreateTitle "dmidecode -t  1"   "_"; $SUDO dmidecode -t 1  | grep -Ei "Manufacturer|Name" >> "${Summary}"
      CreateTitle "dmidecode -t 16"   "_"; $SUDO dmidecode -t 16 | grep  Capacity       >> "${Summary}"
      CreateTitle "dmidecode -t 17"   "_"; $SUDO dmidecode -t 17 | grep -Ei "Size|BANK" >> "${Summary}"
##      CreateTitle "cat /proc/cpuinfo" "_"; cat  /proc/cpuinfo     >> "${Summary}"
       CreateTitle "lscpu " "_"; lscpu| grep -E 'Architecture|Mode|Processeur|Thread|modèle' >> "${Summary}"
      #sudo grep -o -w 'lm' /proc/cpuinfo | sort -u
   fi
}

Uefi () {
  local SecureBoot1 SecureBoot2 Esp Md5Sums EfiPartitions MountPoint
  ## Computer UEFI settings
  CreateTitle "Structure EFI demandée"

  # Boot mode
  [[ -d /sys/firmware/efi ]] && printf "Computer booted in UEFI mode for this session.\n" || printf "Computer booted in Legacy mode for this session.\nUEFI currently not active or not available on this system.\n"
  printf "\n"

  # Secure boot
  [[ -e /sys/firmware/efi/vars/SecureBoot-8be4df61-93ca-11d2-aa0d-00e098032b8c/data ]] && \
  #SecureBoot1=$(${SUDO} od -An -t u1 /sys/firmware/efi/vars/SecureBoot-8be4df61-93ca-11d2-aa0d-00e098032b8c/data | gawk '{ print $1 }')
  SecureBoot1=$(${SUDO} od -An -t u1 /sys/firmware/efi/vars/SecureBoot-*/data | cut -c4)
  [[ -e /sys/firmware/efi/efivars/SecureBoot-8be4df61-93ca-11d2-aa0d-00e098032b8c ]] && \
  #SecureBoot2=$(${SUDO} od -An -t u1 /sys/firmware/efi/efivars/SecureBoot-8be4df61-93ca-11d2-aa0d-00e098032b8c | gawk '{ print $5 }')
  SecureBoot2=$(${SUDO} od -An -t u1 /sys/firmware/efi/efivars/SecureBoot-* | cut -c20)
  # Secure boot mode
  if [[ "${SecureBoot1}" == "1" || "${SecureBoot2}" == "1" ]]; then
    printf "Secure Boot active\n"
  else
    printf "Secure Boot disabled or not available on this system.\n"
  fi
  printf "\n"

  if [[ -d /sys/firmware/efi ]] && hash efibootmgr 2>> "${Trash}"; then
    efibootmgr -v 2>&1;
    printf "\n"
    Esp=$(blkid | grep vfat)
    printf "vfat partition details:\n%s\n\n" "${Esp:-None}"
  fi

  # efi files, md5sum
  if hash lsblk find md5sum 2>> "${Trash}"; then
    Md5Sums=""
    EfiPartitions=$(for device in $(lsblk --noheadings --raw --output name); do ${SUDO} blkid -s PART_ENTRY_TYPE -p /dev/"${device}" 2>> "${Trash}"; done | grep c12a7328-f81f-11d2-ba4b-00a0c93ec93b | cut -d: -f1)
    if [[ -n "${EfiPartitions}" ]]; then
      for device in ${EfiPartitions}; do
        [[ ! -d "${Tempdir}/mnt/$(basename "${device}")" ]] && mkdir -p "${Tempdir}/mnt/$(basename "${device}")"
        MountPoint="${Tempdir}/mnt/$(basename "${device}")"
        Md5Sums+="EFI files in ${device}:\n"
        ${SUDO} mount "${device}" "${MountPoint}"
        Md5Sums+=$(${SUDO} find "${MountPoint}" -type f -iname "*.efi" -exec md5sum {} + | sort)
        Md5Sums=${Md5Sums//${MountPoint}}
        Md5Sums+="\n\n"
        if ${SUDO} test -f "${MountPoint}/EFI/ubuntu/grub.cfg"; then
          Md5Sums+="/EFI/grub/grub.cfg file contents in ${device}:\n"
          Md5Sums+=$(${SUDO} cat "${MountPoint}/EFI/ubuntu/grub.cfg")
        fi
        ${SUDO} umount "${MountPoint}"
      done
    fi
  fi
  printf "${Md5Sums}\n"
}

OsProber () {
  local osprober
  # systems detected by os-prober
  if hash os-prober 2>> "${Trash}"; then
    osprober=$(${SUDO} os-prober)
  fi
  if [[ -n "${osprober}" ]]; then
    CreateTitle "OS prober" "_"
    printf "%s\n\n\n" "${osprober}"
  fi
}

MountBoot () {
  local device, fstab, uuid
  device="${1}"
  if grep -q /boot <<< "$(grep  -Ev '^#|/boot/efi' "${Tempdir}/mnt/$(basename "${device}")"/etc/fstab)"; then
    fstab=$(grep /boot <<< "$(grep  -Ev '^#|/boot/efi' "${Tempdir}/mnt/$(basename "${device}")"/etc/fstab)")
    uuid=$(gawk '{print $1}' <<< "${fstab}")
    ${SUDO} umount -l "${uuid}" > /dev/null
    ${SUDO} mount -o ro "${uuid}" "${Tempdir}/mnt/$(basename "${device}")"/boot &>> "${Trash}"
  fi
}

MountPartitions () {
  local devices
  # live session, mounts available ext2/3/4 root partitions read-only
  # and creates the chroot environnement
  if [[ "${livesession}" -eq 1 ]]; then
    #echo "...live session scheme"
    devices=$(blkid -o device | sed -r '/loop|ram|sr/d')

    for device in ${devices}; do
      [[ ! -d "${Tempdir}/mnt/$(basename "${device}")" ]] && mkdir -p "${Tempdir}/mnt/$(basename "${device}")"
      if mount | grep -qE "${device} on /media|${device} on /mnt"; then
        { ${SUDO} umount "${device}" || ${SUDO} mount -o remount,ro "${device}"; } &>> "${Trash}"
      fi
      if ! (mount | grep -q "${Tempdir}/mnt/$(basename "${device}")"); then
        ${SUDO} mount -o ro "${device}" "${Tempdir}/mnt/$(basename "${device}")" &>> "${Trash}" && \
        if [[ -f "${Tempdir}/mnt/$(basename "${device}")"/etc/fstab && \
              -d "${Tempdir}/mnt/$(basename "${device}")"/dev && \
              -d "${Tempdir}/mnt/$(basename "${device}")"/proc && \
              -d "${Tempdir}/mnt/$(basename "${device}")"/sys && \
              -d "${Tempdir}/mnt/$(basename "${device}")"/run ]]; then
            rootdevices+=${device}" "
        else
          ${SUDO} umount "${Tempdir}/mnt/$(basename "${device}")" 2>> "${Trash}"
        fi
      fi
    done

    for device in ${rootdevices}; do
    echo device $device
      ${SUDO} mount --bind /dev "${Tempdir}/mnt/$(basename "${device}")"/dev
      ${SUDO} chroot "${Tempdir}/mnt/$(basename "${device}")" mount -t proc proc /proc
      ${SUDO} chroot "${Tempdir}/mnt/$(basename "${device}")" mount -t sysfs sysfs /sys
      ${SUDO} chroot "${Tempdir}/mnt/$(basename "${device}")" mount -t devpts devpts /dev/pts
      ${SUDO} chroot "${Tempdir}/mnt/$(basename "${device}")" mount -o mode=755 -t tmpfs tmpfs /run
      ${SUDO} chroot "${Tempdir}/mnt/$(basename "${device}")" mkdir /run/resolvconf
      ${SUDO} chroot "${Tempdir}/mnt/$(basename "${device}")" mount -o mode=1777 -t tmpfs tmpfs /tmp
      ${SUDO} mount --bind /run/resolvconf "${Tempdir}/mnt/$(basename "${device}")/run/resolvconf"
      MountBoot "${device}"
    done
  fi
}

UnmountPartitions () {
  for device in ${rootdevices}; do
    ${SUDO} umount -l "${Tempdir}/mnt/$(basename "${device}")/"{boot,tmp,run/resolvconf,run,dev/pts,sys,proc,dev,} &>> "${Trash}"
  done
}

Disks () {
  CreateTitle "Etat des disques"; CreateTitle "Disks UUID\'s" "_"

  if  [[ "${ano}" -eq 1 ]]; then
      # Checking ext? filesystems health
      devices=$(blkid -o device | sed -r '/loop|ram|sr/d')
      for device in ${devices}; do
         if grep -q "ext[234]" <(blkid "${device}"); then
           if grep state <(tune2fs -l "${device}" 2> /dev/null) 2>> "${Trash}" | grep -qE 'not clean|error'; then
                   fsckrequired+="${device} "
           fi
         fi
      done
      if [[ -n "${fsckrequired}" ]]; then
            CreateTitle "Partitions disques en mauvais état " "*" 
            printf " Les partitions suivantes semblent en mauvais état. Veuillez consulter la documentation de https://doc.ubuntu-fr.org/fsck. \n"
            printf "%s" "${fsckrequired}"
            printf "%s" "${devices}"  
       fi
  fi


 if [[ "${usb}" -eq 1 ]]; then
   echo "==> paramètre USB positionné"
   lsblk --sort name --output name,uuid,fstype,hotplug 
   printf "\n"
   ListeDisque=$(lsblk --sort name --output name,uuid,fstype,hotplug| grep  "                                                     " |cut -c 1-4 )
 fi
 if [[ "${usb}" -eq 0 ]]; then
   echo "==> paramètre USB non positionné"
   lsblk --sort name --output name,uuid,fstype,hotplug  | grep -v "        1"
   printf "\n"
   ListeDisque=$(lsblk --sort name --output name,uuid,fstype,hotplug| grep  "                                                     0"|cut -c 1-4 )
 fi
  CreateTitle "Disks partitions" "_"
# LC_ALL=C ${SUDO} fdisk -l | gawk -v RS= '!/\/dev\/ram|\/dev\/loop/ {print $0, RT}'
  for  Disque  in ${ListeDisque}; do
       printf "\n"
       ${SUDO} fdisk -l /dev/$Disque | grep -v  "Les entrées de la table de partitions ne sont pas dans l'ordre du disque."
  done
  printf "\n"
 }

DiskBootLoader () {
  local Efi=0 hexs512n4 MbrBootLoader CoreimgSector Coreimage="${Tempdir}/coreimage"
  local CoreimageFound=0 Message
  CreateTitle "Structure BOOT demandée. Master Boot Record"
  OsProber >> "${Summary}" ; 
  for disk in ${AtaDisks} ${NvmeDisks} ${MmcDisks} ${UsbDisks}; do
    hexn512=$(${SUDO} hexdump -v -n512 -e '/1 "%02x"' "${disk}")
    hexs512n4=$(${SUDO} hexdump -v -s 512 -n 4 -e '"%_p"' "${disk}")
    if [[ "${hexn512:0:4}" != "0000" ]]; then
      if [[ "${hexn512:0:3}" = "33c08e" ]]; then
        case "${hexn512:0:16}" in
          "33c08ed0bc007cfb" )
            MbrBootLoader="Windows 2000/XP/2003"
            ;;
          "33c08ed0bc007c8e" )
            [[ "${hexn512:480:4}" = "fb54" ]] && MbrBootLoader="Windows Vista"
            [[ "${hexn512:480:4}" = "4350" ]] && MbrBootLoader="Windows 7/8/2012"
            ;;
        esac
      elif [[ "${hexn512:0:4}" = "eb63" ]]; then
        MbrBootLoader="Grub2"
        CoreimgSector=$(${SUDO} hexdump -v -s 92 -n 4 -e '4 "%u"' "${disk}" 2>> "${Trash}")
        ${SUDO} dd if="${disk}" of="${Coreimage}" skip="${CoreimgSector}" count=1 2>> "${Trash}"
        case $(hexdump -v -n 4 -e '/1 "%02x"' "${Coreimage}") in
          '5256be1b' | '5256be6f' | '52e82801' | '52bff481' | '5256be63' | '5256be56' )
            CoreimageFound=1
        esac
      else
        MbrBootLoader="Unmanaged"
      fi
    else
      MbrBootLoader="No"
    fi

    if [[ "${MbrBootLoader}" != "Unmanaged" ]]; then
      Message="${disk}: "
      [[ "${hexs512n4}" = "EFI " ]] && Message+="GPT. "
      Message+="${MbrBootLoader} boot loader is installed in the MBR"
      if [[ "${CoreimageFound}" -eq 1 ]]; then
        Message+=", core.img was found at sector ${CoreimgSector}.\n"
      else
        Message+=".\n"
      fi
      printf "${Message}"
    fi
    Message=""
    Efi=0
    MbrBootLoader=""
    CoreimgSector=""
    CoreimageFound=0
    ${SUDO} rm "${Coreimage}" 2> /dev/null
  done
  printf "\n"
}

OperatingSystem () {
  local SystemctlFailed
  CreateTitle "Operating System: $(sed 's/Description:\t//' <(lsb_release -d))" "_"
  # kernel parameters
  CreateTitle "Kernel parameters" "_"
  cat /proc/cmdline
  printf "\n\n"
  if which systemctl &> /dev/null; then
    # failed services
    SystemctlFailed=$(LC_ALL=C systemctl --state=failed)
    if ! [[ "${SystemctlFailed}" =~ "0 loaded units listed" ]]; then
      CreateTitle "Failed services" "_"
      printf "Failed services:\n"
      printf "${SystemctlFailed}"
      printf "\n\n"
    fi
  fi
}

RootSystem () {
  local RootPartition Path Prefix
  # root partition
  RootPartition="${1}"
  if [[ -n "${RootPartition}" ]]; then
    Path="${Tempdir}/mnt/$(basename "${RootPartition}")"
    # variable used for chrooting
    Prefix="${SUDO} chroot ${Path}"
  fi
  Lsb=$(sed 's/Description:\t//' <(${Prefix} lsb_release -d))
  CreateTitle "${Lsb} in ${RootPartition}"
}

RamUsageAndFreeDiskSpace () {
  # RAM usage, swappiness, resume partition and free disk space
  CreateTitle "RAM usage & Free disk space" "_"
  LC_ALL=C free -h
  printf "Swappiness = $(LC_ALL=C sysctl -n vm.swappiness)\n\n"
  LC_ALL=C df -h --output=source,target,fstype,size,used,avail,pcent,ipcent --exclude=tmpfs --exclude=devtmpfs | sed '1 s/^/000/' | sort | sed '1 s/000//'
  if [[ -f /etc/initramfs-tools/conf.d/resume ]]; then
    printf "\nSwap partition for hibernation : $(LC_ALL=C sed 's/RESUME=//' /etc/initramfs-tools/conf.d/resume 2>> "${Trash}")\n"
  fi
  printf "\n\n"
}

FileContents () {
  local RootPartition Path Prefix
  # root partition
  RootPartition="${1}"
  if [[ -n "${RootPartition}" ]]; then
    Path="${Tempdir}/mnt/$(basename "${RootPartition}")"
    # variable used for chrooting
    Prefix="${SUDO} chroot ${Path}"
  fi
  filecontents="/boot/grub/grub.cfg /etc/default/grub /etc/fstab /etc/crypttab"
  for file in ${filecontents}; do
    if [[ -f "${Path}${file}" ]]; then
      CreateTitle "${file}" "_"
    fi
    if [[ "${file}" = "/boot/grub/grub.cfg" ]] && [[ -f "${Path}${file}" ]]; then
      printf "File size: $(wc -c < "${Path}${file}"), "
      printf "Line count: $(wc -l < "${Path}${file}"), "
      printf "Menu entries (sub entries included) : $(sed "/menuentry /!d" "${Path}${file}" | wc -l)\n\n"
      sed -r '1,/^menuentry/  {/### BEGIN .*10_linux|^menuentry/!d; s/### BEGIN .*10_linux.*/--- Partial file contents\n&\n\n/}' "${Path}${file}" | \
      sed '/^submenu/,/### END .*10_linux/ {/### END .*10_linux/!d; q}'
    else
        sed -r '/^$|^#/d' "${Path}${file}"
    fi
  done
}

EXT4Contents () {
     ##   Rechercher les partitions EXT4:  Soit toutes, soit uniquement celles internes.
     if [[ "${usb}" -eq 1 ]]; then
       CreateTitle "Recherche des éventuels autres O.S. ubuntu: Paramètre USB positionné"  >>"${Summary}" 
       ListeEXT4=$(lsblk --sort name --output name,fstype,hotplug| grep "ext4" |cut -c 1-6 )
     else
       CreateTitle "Recherche des éventuels autres O.S. ubuntu: Paramètre USB non positionné"    >>"${Summary}" 
       ListeEXT4=$(lsblk --sort name --output name,fstype,hotplug| grep  "        0"| grep "ext4"|cut -c 1-6 )
     fi
     local RootPartition Path Prefix
     filecontents="/mnt/boot/grub/grub.cfg /mnt/etc/default/grub /mnt/etc/fstab /mnt/var/log/kern.log"
     for partEXT4 in ${ListeEXT4}; do  
         ${SUDO} mount  -r /dev/${partEXT4}  /mnt  >> "${Summary}"                     
         sleep 5 #### Attention; Attendre deux secondes est insuffisant pour les partitions  USB
         if [[ -f /mnt/etc/fstab ]]; then
             CreateTitle " Structure de boot pour la partition $partEXT4" "_"                           
             for file in ${filecontents}; do
                if [[ "${file}" = "/mnt/boot/grub/grub.cfg" ]] && [[ -f "${Path}${file}" ]]; then
                     printf "File size: $(wc -c < "${Path}${file}"), "
                     printf "Line count: $(wc -l < "${Path}${file}"), "
                     printf "Menu entries (sub entries included) : $(sed "/menuentry /!d" "${Path}${file}" | wc -l)\n\n"
                     sed -r '1,/^menuentry/  {/### BEGIN .*10_linux|^menuentry/!d; s/### BEGIN .*10_linux.*/--- Partial file contents\n&\n\n/}' "${Path}${file}" | \
                          sed '/^submenu/,/### END .*10_linux/ {/### END .*10_linux/!d; q}'
                else
                if   [[ "${file}" = "/mnt/var/log/kern.log" ]] && [[ -f "${Path}${file}" ]]; then
                            printf "\n"
                            CreateTitle " LOGS pour la partition $partEXT4" "_"
                            # Last 25 log entries
                            printf "Last 25 log entries:\n"
                            ${SUDO} tail -25  "${Path}${file}"       ##### | \
                            ####      gawk '{ $4="<filter>"; print $0}' | \
                            ####            sed -r 's/=([[:xdigit:]]{2}:?){6,14}|=([[:digit:]][[:digit:]]?[[:digit:]]?\.?){4}/=<filter>/g'      
                    else
                    CreateTitle "Contenu du fichier $partEXT4${Path}${file}" "_"
                    sed -r '/^$|^#/d' "${Path}${file}"
                    fi
                fi
            done
       else 
            CreateTitle "Pas de partition UBUNTU détectée dans la partition /dev/$partEXT4" "_"
       fi     
       ${SUDO} umount -v /dev/${partEXT4}  >> "${Summary}"; 
       done
}

UsbDevices () {
  # usb devices

 if [[ "${usb}" -eq 1 ]]; then
   echo "==> paramètre USB positionné"
     if hash lsusb 2>> "${Trash}"; then
        CreateTitle "USB devices" "_"
        lsusb
        printf "\n"
        lsusb -t
        printf "\n\n"
        UsbDisks 
     fi
 fi
}

WirelessStatus () {
  # wireless status
  if hash rfkill 2>> "${Trash}"; then
    CreateTitle "Wireless" "_"
    LC_ALL=C rfkill list all
    printf "%s\n\n"
  fi
}

ModulesLoaded () {
  # modules loaded
  if hash lsmod 2>> "${Trash}"; then
    CreateTitle "Graphics Modules Loaded" "_"
    # prints all modules or just graphics modules depending of the command options
    lsmod | sed '1 s/^/000/' | sort | sed '1 s/000//' | grep -i -E 'amdgpu|ati|i915|nouveau|nvidia|radeon'
    printf "\n"
  fi
}

DriversAvailable () {
  # Drivers available
  if hash ubuntu-drivers 2>> "${Trash}"; then
    Drivers=$(ubuntu-drivers devices)
    if [[ -n "${Drivers}" ]]; then
      CreateTitle "Drivers available" "_"
      printf "%s\n\n" "${Drivers}"
    fi
  fi
}

# Locales function
Locales () {
  local RootPartition Path Prefix
  # root partition
  RootPartition="${1}"
  if [[ -n "${RootPartition}" ]]; then
    Path="${Tempdir}/mnt/$(basename "${RootPartition}")"
    # variable used for chrooting
    Prefix="${SUDO} chroot ${Path}"
  fi

  CreateTitle "Locales" "_"
  if [[ -z "$RootPartition" ]]; then
    printf "locale command output:\n"
    ${Prefix} locale
    printf "\n"
  fi
  if [[ -f "${Path}"/etc/default/locale ]]; then
    printf "/etc/default/locale:\n"
    cat ${Path}/etc/default/locale
  fi
  if hash setxkbmap 2>> "${Trash}" && [[ -z "$RootPartition" ]]; then
    printf "\nActive keyboard layout:\n"
    setxkbmap -query
  fi
  if [[ -f "${Path}"/etc/default/keyboard ]]; then
    printf "\nDefault keyboard layout:\n"
    sed '/^#/d; /^$/d' ${Path}/etc/default/keyboard
  fi
  printf "\n"
}

# Packages function
Packages () {
  local RootPartition Path Prefix
  # root partition
  RootPartition="${1}"
  if [[ -n "${RootPartition}" ]]; then
    Path="${Tempdir}/mnt/$(basename "${RootPartition}")"
    # variable used for chrooting
    Prefix="${SUDO} chroot ${Path}"
  fi
  CreateTitle "Repositories" 
    for file in "${Path}"/etc/apt/sources.list "${Path}"/etc/apt/sources.list.d/*; do
      if [[ $(grep --color=never -Ev '^#|^$' "${file}" | wc -l) -ne 0 ]]; then
        printf "Active apt sources in file: ${file//${Path}/}\n"
        grep --color=never -Ev '^#|^$' "${file}"
        printf "\n"
      fi
    done
#fi

  if [[ "${livesession}" -eq 1 ]] && [[ -z "$RootPartition" ]]; then
    :
#  elif [[ -f "${Path}/usr/bin/mintupdate-tool" ]]; then
#  Upgradable=$(LC_ALL=C ${Prefix} mintupdate-tool list 2>> "${Trash}")
#  elif grep -iq LinuxMint "${Path}/etc/lsb-release" 2>> "${Trash}"; then
    # Mint specific
#   :
  elif ${Prefix} which aptitude &>> "${Trash}"; then
    Upgradable=$(LC_ALL=C ${Prefix} aptitude search ?upgradable -F "%p# %V# from:%v#" 2>> "${Trash}")
    Broken=$(LC_ALL=C ${Prefix} aptitude search ?broken 2>> "${Trash}")
  elif ${Prefix} apt list --upgradable &> /dev/null; then
    Upgradable=$(sed '/^.*\//!d; s#\(^.*\)/.[^[:blank:]]* \([[:digit:]].[^[:blank:]]*\) .*from: \([[:digit:]].[^[:blank:]]*\)\]$#\1 \2 from:\3#' <(LC_ALL=C ${Prefix} apt list --upgradable 2>> "${Trash}") | column -t)
  fi
  # packages linux présents 
  CreateTitle "Packages linux présents obtenus par la commande dpkg -l | grep -Ei 'linux-(g|h|i|si|t)' " "_"
  dpkg -l | grep -Ei "linux-(g|h|i|si|t)" | sort -k3 >> "${Summary}"
  # upgradable packages
  if [[ -n "${Upgradable}" ]]; then
    CreateTitle "Upgradable Packages" "_"
    printf "%s\n\n" "${Upgradable}"
  fi
  # broken packages
  if [[ -n "${Broken}" ]]; then
    CreateTitle "Broken Packages" "_"
    printf "%s\n\n" "${Broken}"
  fi
  # lists interrupted package configuration
  if ${Prefix} which dpkg &>> "${Trash}"; then
    ConfigPending=$(LC_ALL=C ${Prefix} dpkg --simulate --configure -a 2> /dev/null)
    if [[ -n "${ConfigPending}" ]]; then
      CreateTitle "Package Configuration Pending" "_"
      printf "%s\n\n" "${ConfigPending}"
    fi
  fi
  # lists interrupted package installation
  if ${Prefix} which apt-get &>> "${Trash}"; then
     InstallPending=$(LC_ALL=C ${Prefix} apt-get install --simulate)
     [[ "${InstallPending}" =~ "0 upgraded, 0 newly installed, 0 to remove" ]] && InstallPending=""
     if [[ -n "${InstallPending}" ]]; then
       CreateTitle "Package Installation Pending" "_"
       printf "%s\n\n" "${InstallPending}"
     fi
  fi
  # lists removed packages with configuration files still present
  if ${Prefix} dpkg -l | grep ^rc &> /dev/null; then
       ##### RcPackages=$(grep ^rc <(LC_ALL=C ${Prefix} dpkg -l 2> /dev/null) | gawk '{print $2, $3}' | sed '1 s/^/Package Version\n/' | column -t)
       RcPackages=$(grep ^rc <(LC_ALL=C ${Prefix} dpkg -l 2> /dev/null) )
    if [[ -n "${RcPackages}" ]]; then
      CreateTitle "Configuration files of uninstalled packages" "_"
      printf "%s\n\n" "${RcPackages}"
    fi
  fi
}

# Logs function
Logs () {
  local RootPartition Path Prefix
  # root partition
  RootPartition="${1}"
  if [[ -n "${RootPartition}" ]]; then
    Path="${Tempdir}/mnt/$(basename "${RootPartition}")"
    # variable used for chrooting
    Prefix="${SUDO} chroot ${Path}"
  fi

  # Logs section
  CreateTitle "Kernel Logs" "_"
  # Error log entries
  printf "Error log entries:\n"
  if [[ -z "${RootPartition}" ]]; then
    LC_ALL=C ${SUDO} dmesg --time-format reltime | grep -i -E ' bug|error|exception|failed|firmware|missing|reset|segfault|timeout' | \
    tail -25 | \
    sed -r 's/=([[:xdigit:]]{2}:?){6,14}|=([[:digit:]][[:digit:]]?[[:digit:]]?\.?){4}/=<filter>/g' || \
    printf "None\n"
  else
    [[ -f "${Path}"/var/log/syslog ]] && \
    ${SUDO} grep -iE ' bug|error|exception|failed|firmware|missing|reset|segfault|timeout' "${Path}"/var/log/kern.log | \
    tail -25   #### |              \
    #gawk '{ $4="<filter>"; print $0}' | \
    sed -r 's/=([[:xdigit:]]{2}:?){6,14}|=([[:digit:]][[:digit:]]?[[:digit:]]?\.?){4}/=<filter>/g' || \
    printf "None\n"
  fi
  printf "\n\n"
  # Last 25 log entries
  printf "Last 25 log entries:\n"
  if [[ -z "${RootPartition}" ]]; then
    ${SUDO} dmesg | tail -25 | \
    sed -r 's/=([[:xdigit:]]{2}:?){6,14}|=([[:digit:]][[:digit:]]?[[:digit:]]?\.?){4}/=<filter>/g'
  else
    [[ -f "${Path}"/var/log/syslog ]] && ${SUDO} tail -25 "${Path}"/var/log/kern.log | \
    gawk '{ $4="<filter>"; print $0}' | \
    sed -r 's/=([[:xdigit:]]{2}:?){6,14}|=([[:digit:]][[:digit:]]?[[:digit:]]?\.?){4}/=<filter>/g'
    printf "None\n"
  fi
  printf "\n\n"
}

# Xorg function
Xorg () {
  CreateTitle "Xorg" "_"
  local RootPartition Path Prefix
  # root partition
  RootPartition="${1}"
  if [[ -n "${RootPartition}" ]]; then
    Path="${Tempdir}/mnt/$(basename "${RootPartition}")"
    # variable used for chrooting
    Prefix="${SUDO} chroot ${Path}"
  fi
  printf "Error log entries:\n"
  grep -E '\(EE' ${Path}/var/log/Xorg.0.log || printf "None\n"
  printf "\n"
  printf "Last 25 log entries:\n"
  if [[ -f "${Path}"/var/log/Xorg.0.log ]]; then
    iconv -c ${Path}/var/log/Xorg.0.log | tail -25
    printf "\n\n"
  else
    printf "None\n\n"
  fi
}

GpuManager () {
  CreateTitle "GPU Manager" "_"
  local RootPartition Path Prefix
  # root partition
  RootPartition="${1}"
  if [[ -n "${RootPartition}" ]]; then
    Path="${Tempdir}/mnt/$(basename "${RootPartition}")"
    # variable used for chrooting
    Prefix="${SUDO} chroot ${Path}"
  fi
  if [[ -f "${Path}"/var/log/gpu-manager.log ]]; then
    printf "$(LC_ALL=C date -r ${Path}/var/log/gpu-manager.log -I), "
    printf "GPU Manager logs:\n"
    sed '/[Nn]o$/d' ${Path}/var/log/gpu-manager.log
    printf "\n\n"
  fi
  if ${Prefix} dpkg-query -l "nvidia-*" 2> /dev/null | grep -q ^ii; then
    printf "Nvidia packages and kernel modules:\n"
    { ${Prefix} dpkg-query -l "nvidia-*" 2> /dev/null; ${Prefix} dpkg-query -l "bbswitch*" 2> /dev/null; } | grep ^ii | gawk '{print $2, $3}' | column -t
    printf "\n"
    { LC_ALL=C ${Prefix} find /lib/modules -type f -iname "*nvidia*" -exec ls -n {} +; LC_ALL=C ${Prefix} find /lib -maxdepth 1  -type d -iname "nvidia*" -exec ls -nd {} +; } | sed 's/[[:blank:]][[:blank:]]*/ /g' | cut -d' ' -f6-
    printf "\n\n"
  fi
}

SmartSummary () {
  local Smart Smartcde SmartctlOutput
  if hash smartctl 2>> "${Trash}"; then
    CreateTitle "Disks SMART Attributes"
    Smart=""
    Smartcde="${SUDO} smartctl -s on -iHA --log=error -f brief -q noserial"

    if [[ -n "${NvmeDisks}" ]]; then
#      Smart+="### NVMe disks: ${NvmeDisks}\n\n"
      for disk in ${NvmeDisks}; do
        CreateTitle "NVMe Disks SMART Attributes $disk " "-" 
        ${Smartcde} "${disk}" 2>> "${Trash}"
        SmartctlOutput=$(${Smartcde} "${disk}" 2>> "${Trash}" | sed -r '/0\/0$| 0$/d')
#        [[ ! "${SmartctlOutput}" =~ "Read Device Identity failed" ]] && Smart+="# Drive ${disk}:\n${SmartctlOutput}\n\n"
#        printf "${Smart}";  printf "\n"
        printf "$SmartctlOutput";  printf "\n" 
      done
    fi
    if [[ -n "${MmcDisks}" ]]; then
#     Smart+="### MMC disks: ${MmcDisks}\n\n"
      for disk in ${MmcDisks}; do
          CreateTitle "eMMC Disks SMART Attributes $disk " "-" 
          ${Smartcde} "${disk}" 2>> "${Trash}"
          SmartctlOutput=$(${Smartcde} "${disk}" 2>> "${Trash}" | sed -r '/0\/0$| 0$/d')
#         [[ ! "${SmartctlOutput}" =~ "Read Device Identity failed" ]] && Smart+="# Drive ${disk}:\n${SmartctlOutput}\n\n"
#          printf "${Smart}";  printf "\n"
          printf "$SmartctlOutput";  printf "\n" 
      done
    fi
    if [[ -n "${AtaDisks}" ]]; then
       for disk in ${AtaDisks}; do
           CreateTitle "ATA Disks SMART Attributes $disk " "_" 
           SmartctlOutput=$(${Smartcde} "${disk}" 2>> "${Trash}" | sed -r '/0\/0$| 0$/d')
#           [[ ! "${SmartctlOutput}" =~ "Read Device Identity failed" ]] && Smart+="\n${SmartctlOutput}\n" 
#           printf "${Smart}";  printf "\n"
            printf "$SmartctlOutput";  printf "\n"    
       done
    fi
    if [[ "${usb}" -eq 1 ]]; then
          ##  echo "==> paramètre USB positionné"; echo "Liste des disques externes   en connectique USB= ${UsbDisks}"
          if [[ -n "${UsbDisks}" ]]; then
             Smart+="\n\n### USB disks: ${UsbDisks}\n\n"
             for disk in ${UsbDisks}; do
                  CreateTitle "USB Disks SMART Attributes $disk " "_" 
                  ## [[ ! "${SmartctlOutput}" =~ "Read Device Identity failed" ]] && Smart+="# Drive ${disk}:\n${SmartctlOutput}\n\n"
                            dtype=$(${SUDO}  smartctl --scan | grep -i  $disk | awk '{print $3}')                        
                            echo "Type de boîtier ou d'adaptateur: $dtype"
                            if [[ "${dtype}" = "sat" ]]; then
                                 for soustype in  sat sat,16 sat,auto ; do
                                     CreateTitle "${SUDO} smartctl -d ${soustype} -s on -T verypermissive -iHA --log=error -f brief -q noserial   $disk" "_"   
                                     ${SUDO} smartctl -d ${soustype} -s on -T verypermissive -iHA --log=error -f brief -q noserial $disk 2>> "${Trash}" | sed -r '/0\/0$| 0$/d'                
                                 done
                            elif 
                                  [ $( echo "${dtype}" | grep -cE 'ata|scsi|auto|test|marvell|usbcypress|usbjmicron|usbsunplus' ) -eq 1 ]; then
                                  echo "${SUDO} smartctl -d $dtype -s on -T verypermissive -iHA --log=error -f brief -q noserial   $disk"
                                  ${SUDO} smartctl -d $dtype -s on -T verypermissive -iHA --log=error -f brief -q noserial $disk 2>> "${Trash}" | sed -r '/0\/0$| 0$/d'
                            else                                
                                  for Ztype in  ata sat sat,16 sat,auto scsi auto test marvell usbcypress usbjmicron usbsunplus ; do
                                      CreateTitle "${SUDO} smartctl -d $Ztype -s on -T verypermissive -iHA --log=error -f brief -q noserial   $disk"  "_"  
                                      ${SUDO} smartctl -d $Ztype -s on -T verypermissive -iHA --log=error -f brief -q noserial $disk 2>> "${Trash}" | sed -r '/0\/0$| 0$/d'               
                                  done 
                            fi          
            done
         fi
     fi 
     if  [[ "${ano}" -eq 1 ]]; then
         ## Reallocated sectors
         [[ $(sed -r '/^=+   Disks SMART/,$!d' "${Summary}" | grep -iE ".*5 Reallocated") ]] &&\
         echo "Des secteurs réalloués sont présents dans un disque: Surveillez leur évolution."; echo

          ## Pending sectors
          [[ $(sed -r '/^=+   Disks SMART/,$!d' "${Summary}" | grep "^197 Current_Pending_Sector") ]] &&\
          echo "Des secteurs illisibles sont présents dans un disque: Il faut prendre tranquillement le temps de traiter ce problème." ; echo

          ## Offline Uncorrectable sectors   ### Me semble inutile a signaté car en lien direct avec les deux cas précédants
          ##[[ $(sed -r '/^=+   Disks SMART/,$!d' "${Summary}" | grep "^198 Offline_Uncorrectable") ]] &&\
          hints+="- The hard disk SMART Attibutes shows uncorrectable bad blocks.\n A high value might indicate a mechanical wear or a degradation of the storage surface.\n\n"
          ## G-Sense_Error_Rate ### Me semble être de l'affolement inutile.
          ##[[ $(sed -r '/^=+   Disks SMART/,$!d' "${Summary}" | grep "^191 G-Sense_Error_Rate") ]] &&\
          ##hints+="- The hard disk SMART Attibutes shows the shock-sensitive sensors recorded errors as a result of impact loads.\n This might damage the hard disk, take care.\n\n"  
           
          ## FAILLING KNOW
          [[ $(sed -r '/^=+   Disks SMART/,$!d' "${Summary}" | grep "^FAILING_NOW") ]] &&\
          echo "Un disque est en très mauvais état: Il faut intervenir assez rapidement (dans la semaine ?) pour traiter ce problème." ; echo     
     fi  
  else
      if [[ "${ano}" -eq 1 ]]; then
            CreateTitle "Disks SMART Attributes"
            echo "L'application smartctl n'a pas été détectée dans cet O.S.  Il est conseillé de suivre l'évolution de  l'état des disques."
            echo "             Cela s'installe avec la commande 'sudo apt install --no-install-recommends smartmontools' . "
            echo "                 C'est bien expliqué dans cette documentation https://doc.ubuntu-fr.org/smartmontools ."  
      fi
  fi
##  printf "${Smart}"
##  printf "\n"
}

############# Hints ()
{
  local NVgpu Igpu hints
  ### Hints
  ## Nvidia
  NVgpu=$(sed -r '/^_+   Drivers/,/^_+   Kernel/!d' "${Summary}" | grep recommended$ | awk '{print $3}')
  [[ "${NVgpu}" =~ "nvidia-" ]] && [[ $(sed -r '/^=+   UEFI/,/^=+   Disks/!d' "${Summary}") =~ (Secure Boot active) ]] && hints+="- Nvidia GPU detected, disable Secure Boot in the computer setup to install\n  the Nvidia proprietary driver.\n\n"

  if [[ "${NVgpu}" =~ "nvidia-" ]] && \
     ! grep ^Status <(dpkg-query --status "${NVgpu}") | grep -q "install ok installed" 2>> "${Trash}"; then
    hints+="- You may install the recommended proprietary driver ${NVgpu}.\n  See the driver's application interface.\n\n"
  fi

  ## Intel GPU
  Igpu=$(sed -r '/^=+   inxi/,/^=+   UEFI/!d' "${Summary}")
  if [[ $(grep ^System <<< "${Igpu}") =~ (Kernel: 4.[89].0) || \
        $(grep ^System <<< "${Igpu}") =~ (Kernel: 4.1[012].0) ]] && \
     [[ $(grep ^CPU <<< "${Igpu}") =~ (Intel Core i[357]) ]] && \
     grep ^Status <(dpkg-query --status xserver-xorg-video-intel) | grep -q "install ok installed" 2>> "${Trash}"; then
    hints+="- Intel Core i3/5/7 CPU detected with a recent kernel, it is recommended\n  to use the built-in Xorg modesetting driver and uninstall the legacy Intel\n  driver. Use the following command:\n  sudo apt remove xserver-xorg-video-intel\n\n"
  fi

  # Intel cpu microcode
  if [[ "${Drivers}" =~ "intel-microcode" ]] && \
     ! $(grep ^Status <(dpkg-query --status intel-microcode) | grep -q "install ok installed") 2>> "${Trash}"; then
    hints+="- You may install the Intel CPU microcode update available for your CPU.\n  See the driver's application interface.\n\n"
  fi

  # AMD cpu microcode
  if [[ "${Drivers}" =~ "amd64-microcode" ]] && \
     ! $(grep ^Status <(dpkg-query --status amd64-microcode) | grep -q "install ok installed" 2>> "${Trash}"); then
    hints+="- You may install the AMD CPU microcode update available for your CPU.\n  See the driver's application interface.\n\n"
  fi
 
  if [[ -n "${hints}" ]]; then
    echo "[code]"  >> "${Summary}"
    CreateTitle "Les points suivants méritent votre attention" >> "${Summary}"
    echo -e "${hints}" >> "${Summary}"
   # printf "\n"
    echo "[/code]"  >> "${Summary}"
  fi
}

##########===============================================
GrubResume () {
  /usr/bin/gawk  'BEGIN {                                                                                                                       
       l=0                                                                                                                                
       menuindex= 0                                                                                                                       
       stack[t=0] = 0                                                                                                                     
       }                                                                                                                                    
  function push(x) { stack[t++] = x }                                                                                                  
  function pop() { if (t > 0) { return stack[--t] } else { return "" }  }                                                              
  {                                                                                                                                    
  if( $0 ~ /.*menuentry.*{.*/ )                                                                                                             
  {                                                                                                                                    
      push( $0 )                                                                                                                         
      l++;                                                                                                                               
  } else if( $0 ~ /.*{.*/ )                                                                                                            
  {                                                                                                                                    
    push( $0 )                                                                                                                         
  } else if( $0 ~ /.*}.*/ )                                                                                                            
  {                                                                                                                                    
     X = pop()                                                                                                                          
     if( X ~ /.*menuentry.*{.*/ )                                                                                                            
    {                                                                                                                                  
       l--;                                                                                                                            
       match( X, /^[^'\'']*'\''([^'\'']*)'\''.*$/, arr )                                                                               
       if( l == 0 )                                                                                                                    
      {                                                                                                                               
       print menuindex ": " arr[1]                                                                                                   
       menuindex++                                                                                                                   
       submenu=0                                                                                                                     
      } else                                                                                                                          
     {                                                                                                                               
       print "  " (menuindex-1) ">" submenu " " arr[1]                                                                               
       submenu++                                                                                                                     
     }                                                                                                                               
    }                                                                                                                                  
  }                                                                                                                                    
 }' $1
}
##############
FileResume () {
   CreateTitle "Autres partitions" "_"
   ListeEXT4=$(lsblk --sort name --output name,fstype,hotplug| grep  "        0"| grep "ext4"|cut -c 1-6 )
     for partEXT4 in $ListeEXT4; do  
         ${SUDO} mount  -r /dev/${partEXT4}  /mnt  >> "${Summary}"                     
         sleep 2 #### Attention; Attendre deux secondes est insuffisant pour les partitions  USB
         if [[ -f /mnt/boot/grub/grub.cfg ]]; then
                     printf "Ubuntu présent dans la partition /dev/$partEXT4    "
                     printf "File size: $(wc -c < /mnt/boot/grub/grub.cfg), "
                     printf "Line count: $(wc -l < /mnt/boot/grub/grub.cfg), "
                     printf "Menu entries (sub entries included) : $(sed "/menuentry /!d" /mnt/boot/grub/grub.cfg | wc -l)\n"
                     grep  "boot/vmlinuz"  < /mnt/boot/grub/grub.cfg 
                     printf "\n"                         
        fi     
        ${SUDO} umount -v /dev/${partEXT4}  >> "${Summary}"; 
    done
}

##################################################################################################################
echo DEMARRAGE
printf "\n  Le fichier est disponible ici:  ${Summary}" >> "${Summary}"
printf "\n\n  Lire le compte-rendu dans votre éditeur favori et éliminer/masquer de ce compte-rendu, ce que vous ne souhaitez pas montrer.  "  >> "${Summary}"
printf "\n\n  Faites un copier ( Ctrl a / Ctrl c )  puis un coller (Ctrl v) de ce qui reste dans la discussion ouverte pour votre problème.\n\n\n\n\n">> "${Summary}"
  while [[ ${#} -ge 1 ]]; do
    key="${1}"
    case "${key}" in
        --ano)
        ano=1
        shift
        ;;
      --luks)
        luks=1
        shift
        ;;
      --usb)
        usb=1
        shift
        ;;
       BOOT)
            echo "Structure  de BOOT demandée";  DiskBootLoader >> "${Summary}" ;
            if [[ "${livesession}" -eq 1 ]]; then
                 MountPartitions
                 RamUsageAndFreeDiskSpace >> "${Summary}"
                 ModulesLoaded >> "${Summary}"
                 DriversAvailable >> "${Summary}"
                 [[ "${usb}" -eq 1 ]] && printf "..." && UsbDevices >> "${Summary}"
                  for rootpart in ${rootdevices}; do  
                      RootSystem "${rootpart}" >> "${Summary}"
                      FileContents "${rootpart}" >> "${Summary}"
 #######                   [[ "${locales}" -eq 1 ]] && printf "..." && Locales "${rootpart}" >> "${Summary}"
                       Locales "${rootpart}" >> "${Summary}"
                       Packages "${rootpart}" >> "${Summary}"
                       Logs "${rootpart}" >> "${Summary}"
                       Xorg "${rootpart}" >> "${Summary}"
  ######                   [[ "${gpu}" -eq 1 ]] && printf "..." && GpuManager "${rootpart}" >> "${Summary}"
                       GpuManager "${rootpart}" >> "${Summary}"
                   done
                 UnmountPartitions
             else
                 echo "                          Système opérationnel" >>"${Summary}"
                 OperatingSystem >> "${Summary}"  
                 RamUsageAndFreeDiskSpace >> "${Summary}"
                 FileContents >> "${Summary}"
                 [[ "${locales}" -eq 1 ]] && printf "..." && Locales >> "${Summary}"
                 ModulesLoaded >> "${Summary}"
                 DriversAvailable >> "${Summary}"
                 [[ "${usb}" -eq 1 ]] && UsbDevices >> "${Summary}"
                 Logs >> "${Summary}"
                 Xorg >> "${Summary}"
                 echo "                            Autres systemes: Prevoir CINQ secondes par partition inspectée"; EXT4Contents >> "${Summary}" ;  
            fi
            shift
            ;;
        DISQUE | DISQUES | DISKS | DISK)
            echo "structure DISQUES demandée" ; Disks >> "${Summary}" ;  SmartSummary >> "${Summary}" 
            shift
            ;;
       EFI)
            echo "structure EFI demandée" ; Uefi >> "${Summary}"  
            shift
            ;;
       MATOS)
            echo "état du matériel demandé"; InxiSummary >> "${Summary}"
            shift
            ;;
        PACK)
            echo "liste des packages demandée" ; Packages >> "${Summary}"  
            shift
            ;;
        RESUME) 
            echo "Résumé. Nécessite 2 secondes d'exécution par partition présente."; CreateTitle "Bref résumé" >> "${Summary}"; CreateTitle "Nature du boot" "_" >> "${Summary}"
            [ -d /sys/firmware/efi ] && echo "Session démarrée en mode boot EFI" >> "${Summary}"|| echo "Session démarrée en mode boot non-EFI" >>"${Summary}"
            CreateTitle "Type de processeur"  "_" >>"${Summary}" ; lscpu | grep -Ei "Archi|Mode" >> "${Summary}"
            CreateTitle "Taille de la mémoire utilisée"  "_" >>"${Summary}" ; free -h >> "${Summary}"
            CreateTitle "Gestion de la mémoire:" "_" >>"${Summary}" ; cat /proc/swaps >>"${Summary}" ; cat /etc/sysctl.d/99-swappiness.conf >>"${Summary}"
            CreateTitle "Test de Refind:" "_" >>"${Summary}" ; dpkg -l refind | grep refind >> "${Summary}"
            CreateTitle "Version de l'O.S.:" "_" >>"${Summary}"; lsb_release -cds >>"${Summary}"; cat /proc/cmdline >>"${Summary}"
            CreateTitle "Structure de démarrage:" "_" >>"${Summary}" ; cat /etc/initramfs-tools/conf.d/resume >>"${Summary}" ; efibootmgr -v >>"${Summary}" 
            CreateTitle "Session graphique:"  "_" >>"${Summary}" ; echo $XDG_CURRENT_DESKTOP >> "${Summary}"; echo $XDG_SESSION_TYPE >>"${Summary}" 
 ########## CreateTitle "Les Opérating System présents:"  "_" >>"${Summary}" ; echo OsProber >> "${Summary}" >>"${Summary}" 
            CreateTitle "Les gros répertoires:" "_">>"${Summary}"
                   df -lh --type ext4 >>"${Summary}"
                   df -li --type ext4 >>"${Summary}"
                   du -xm /boot    2>/dev/null | sort -nr  | head -5 >>"${Summary}"
                   du -xm /var/log 2>/dev/null | sort -nr  | head -5 >>"${Summary}" 
                   du -xm /home    2>/dev/null | sort -nr  | head -5 >>"${Summary}" 
           CreateTitle "Le temps de démarrage:" "_" >>"${Summary}" ; systemd-analyze critical-chain >>"${Summary}"
           CreateTitle "Organisation des menus Grub :" >>"${Summary}"
                 grub-install --version >>"${Summary}"
                 GrubResume  /boot/grub/grub.cfg >> "${Summary}"
                 FileResume  >> "${Summary}"
           CreateTitle "Organisation des partitions :" >> "${Summary}"
                /bin/lsblk -apo name,fstype,partlabel,size,label,alignment,mountpoint,uuid,MODEL | grep -v "loop" >>"${Summary}" 
           shift
           ;;
       -v | -V | --version)
        Version
        ;;
      -h | -help | --help	| -* | help)
        Help
        ;;
      *)
        [[ -z "${SummaryFile}" ]] && SummaryFile="${1}" || Help
        shift
        ;;
    esac
  done

printf "[/code]" >> ${Summary} 
printf "\n  Le fichier est disponible ici:  ${Summary}" 
printf "\n  Lire le compte-rendu dans votre éditeur favori et éliminer/masquer de ce compte-rendu, ce que vous ne souhaitez pas montrer.  " 
printf "\n  Faites un copier ( Ctrl a / Ctrl c )  puis un coller (Ctrl v) de ce qui reste dans la discussion ouverte pour votre problème. \n"
xdg-open "${Summary}" 
#### FIN
